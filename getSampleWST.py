#!/usr/bin/env python
__author__ = 'Oleg'

import http.client
import sys
import requests
import json
import os
# import python_http_client   rjr- not sure why in my environment I needto comment this out


def getSampleWST():
    r = requests.get("http://claritydev1.jgi-psf.org/sequencing-projects/1199234/samples")
    status = r.status_code
    header = r.headers['content-type']
    content = r.content
    text = r.text
    jsonstring = r.json()
    # see different parts of the reposnse
    print(status, header)
    # print json
    print(json.dumps(jsonstring, sort_keys=True, indent=4))

    print("sample metadata")
    # jsonstring has a key = 'samples" whose value is a list, each element in the list is a dict
    for key, samplelist in jsonstring.items():
        if key == 'samples':
            print("=======================")
            print("number of samples: " +str(len(samplelist)))
#            print(type(samplelist))
            for sample in samplelist:
                print("sample id: " + str(sample.get('sample-id')))
                print("status: " + sample.get('current-sample-status'))
                print("sample-receipt-date: " + str(sample.get('sample-receipt-date')))
                print("current-sample-status: " + str(sample.get('current-sample-status')))
                print("sample-current-mass-ng: " + str(sample.get('sample-current-mass-ng')))
                print("sample-qc-status: " + str(sample.get('sample-qc-status')))
                print("max-insert-size-kb: " + str(sample.get('max-insert-size-kb')))
                print("plated-sample: " + str(sample.get('plated-sample')))
                print("tube-plate-label: " + str(sample.get('tube-plate-label')))
                print("group-name: " + str(sample.get('group-name')))
                print("qc-comments: " + str(sample.get('qc-comments')))
                print("external: " + str(sample.get('external')))
                print("sample qc: " + str(sample.get('sample-qc-type')))
                print("OK from web service")
                print("=======================")
    # another way to do same thing
    allSamples = jsonstring.get(key)
    print(len(allSamples))

    print("================")


getSampleWST()
