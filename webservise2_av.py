#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json

def webservice2_av():
    r = requests.get('http://claritydev1.jgi-psf.org/sequencing-projects/1104935/library-stocks')
    status =  r.status_code
    header =  r.headers['content-type']
    content = r.content
    text = r.text
    jsonstring = r.json()
    # see different parts of the reposnse
    print(status, header)
    #print(content)
    #print(text)
    print(jsonstring)
    #use this
    #print(json.dumps(jsonstring, sort_keys=True, indent=4))
    #  get type of object:    print(type(jsonstring))


    #jsonstring has a key = 'samples" whose value is a list, each element in the list is a dict
    for key, samplelist in jsonstring.items():
        if key == 'samples':
            print (len(samplelist))
            print (type(samplelist))
            for sample in samplelist:
                print( "sample id: " + str(sample.get('sample-id')))
                print("status: " + sample.get('current-sample-status'))
                print("sample qc: " + str(sample.get('sample-qc-type')))
                print("OK from Becky")

    #another way to do same thing
 #   allSamples = jsonstring.get('samples')
 #   print(len(allSamples))
 #   print(type(allSamples))
 #   for sample in allSamples:
 #       print("sample id: " + str(sample.get('sample-id')))
                #       print("status: " + sample.get('current-sample-status'))
                #      print("plate?: " + str(sample.get('plated-sample')))


webservice2_av()