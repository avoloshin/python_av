#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'

import requests
#import json

#name:  sowItemsWST  - sow Items Web Service Test
#code verifies info on requirements doc:  https://docs.google.com/document/d/1Gc8cR8NLU70gHhfKFGvoWspnJwdM0lCnoXzELRtsrvw/edit
#this program, runs the ws and verifies that the attributes returned in response are the same as documented in
#requirements doc.  Also checks that return status is correct when invalid request is given

def sowItemsWST():
    #use spID as a variable for input later
    spID = 1104935
    requestURL = 'http://claritydev1.jgi-psf.org/sequencing-projects/' + str(spID) + '/sow-items'
    r = requests.get(requestURL)
    status = r.status_code
    print("---- TEST 1: VALID REQUEST,   EXPECT STATUS = 200 and JSON response ----")
    print("---- verify all attributes are retrieved ----")
    if status==200:
        print("Returned Status = ", status)
        jsonstring = r.json()
        # this prints all the fields as a json object
        # print(json.dumps(jsonstring, sort_keys=True, indent=4))
        #now grab individual attributes - just a few
        for key, retrievedList in jsonstring.items():
            if key == 'sow-items':
                print("number of sow items = " + str(len(retrievedList)))
                print("")
                for sowItem in retrievedList:
                    print("sow-item-id: " + str(sowItem.get('sow-item-id')))
                    print("current-status: " + sowItem.get('current-status'))
                    print("run-mode: " + sowItem.get('run-mode'))
                    print("sequencer-model: " + sowItem.get('sequencer-model'))
                    print("platform: " + sowItem.get('platform'))
                    print("sample-id: " + str(sowItem.get('sample-id')))
                    print("sample-name: " + sowItem.get('sample-name'))
                    print("gls-sample-id(clarity_sample_limsid): " + sowItem.get('gls-sample-id'))
                    print("sequencing-project-id: " + str(sowItem.get('sequencing-project-id')))
                    print("library codes:" + str(sowItem.get('library-codes')))
                    print ("")
    else:
        print("Unexpected Returned Status = ", status)

     #set up a dictionary (key, value) of expected attributes (note just using the key; 'value' is of no interest)
    expectedAttributes = { 'sow-item-id': 0, 'sow-item-type':0,'current-status':0,
                           'status-date': 0,'target-logical-amount': 0,'completed-logical-amount': 0,
                           'total-logical-amount-completed': 0,'logical-amount-units': 0,
                           'degree-of-pooling': 0,'run-mode': 0,'sequencer-model': 0,
                           'platform': 0,'sample-id': 0,'sample-name': 0,'gls-sample-id': 0,
                           'sequencing-project-id': 0,'sequencing-product': 0,'material-type': 0,
                           'target-insert-size-kb': 0,'target-template-size-bp': 0,'target-fragment-size-bp':0,
                           'tight-insert': 0,'overlapping-reads': 0,'rrna-depletion': 0,
                           'poly-a-selection': 0,'itag-primer-set': 0,'library-protocol': 0,
                           'exome-capture-probe-set': 0,'sm-instructions': 0,'lc-instructions': 0,
                           'sq-instructions': 0,'library-creation-specs': 0,'library-codes': 0}

    #verify that all the expected attributes are returned in response of WS call
    for key, retrievedList in jsonstring.items():  #for each sow item, check that  all attributes that are required (per contract) are in WS response
        if key == 'sow-items':
            print("number of sow items = " + str(len(retrievedList)))
            print("")
            for sowItem in retrievedList:
                print("sow-item-id: " + str(sowItem.get('sow-item-id')))
                for attributeName in expectedAttributes:
                    if attributeName not in sowItem:
                        print("Error. Not Found. Expecting attribute in sowItem= ", attributeName)


    # now go the other way and verify that all the returned  attributes are the same as the expected attributes (per contract)
    for key, retrievedList in jsonstring.items():
        if key == 'sow-items':
            print("number of sow items = " + str(len(retrievedList)))
            print("")
            for sowItem in retrievedList:
                print("sow-item-id: " + str(sowItem.get('sow-item-id')))
                for attributeName in sowItem:
                    if attributeName not in expectedAttributes:
                        print("Error, Unexpected attribute returned in sowItem= ", attributeName)

    # to do: confirm fields with values from database


    # check return  error codes when request is wrong
    #invalid or missing sp-id or incorrect URL, expect 404
    print("---- TEST 2: verify INVALID REQUEST,   EXPECT STATUS = 404 ----")
    spID = 0
    requestURL = 'http://claritydev1.jgi-psf.org/sequencing-projects/' + str(spID) + '/sow-items'
    r = requests.get(requestURL)
    status = r.status_code
    if status == 404:
        print("Expected Returned status for invalid request = ", status)

##to do later is update this code because the requirements were updated

sowItemsWST()


#history -
# 5-19-17 this test was successful in finding a bug with documentation (attribute was not listed that actually was retrieved)
# created   PPS-3832 (doc: retrieve sow-items for SP WS returns unexpected attribute)
# 6-30-17 requirements doc updated and code likewise.  all Tests passed