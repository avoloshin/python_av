#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'

import requests
import json

#name:  sowItemTemplatesWST  - sow Items Templates Web Service Test
#code verifies info on requirements doc:  https://docs.google.com/document/d/1UXW7BEIbzy8XWuiIvLk7W6lKtLTFWwckkpm_fsALnok/edit
#this program, runs the ws and verifies that the attributes returned in response are the same as documented in
#requirements doc. Checks that return status is correct when invalid request is given. Checks that correct return status for HTTP calls

def sowItemTemplatesWST():
    #use spID as a variable for input later
    spID = 1104935
    requestURL = 'http://claritydev1.jgi-psf.org/sequencing-projects/' + str(spID) + '/sow-item-templates'
    r = requests.get(requestURL)
    status = r.status_code
    print(" ")
    print("---- TEST 1: VALID REQUEST,   EXPECT STATUS = 200 and JSON response ----")
    print("---- verify all attributes are retrieved ----")
    if status==200:
        print("Returned Status = ", status)
        jsonstring = r.json()
        # print(jsonstring)
        # this prints all the fields as a json object
        print(json.dumps(jsonstring, sort_keys=True, indent=4))
        #now grab individual attributes - just a few
        '''
        for key, retrievedList in jsonstring.items():
            if key == 'sow-item-templates':
                print("number of sow item templates = " + str(len(retrievedList)))
                print("")

                for sowItemTemplate in retrievedList:
                    print("sow-number: " + str(sowItemTemplate.get('sow-number')))
                    print("sow-item-type: " + sowItemTemplate.get('sow-item-type'))
                    print ("")
        '''
    else:
        print("Unexpected Returned Status = ", status)

     #set up a dictionary (key, value) of expected attributes (note just using the key; 'value' is of no interest)
    expectedAttributes = { 'sow-number': 0, 'sow-item-type':0,'sequencing-product':0,
                           'platform': 0,'sequencer-model': 0,'target-logical-amount': 0,
                           'logical-amount-units': 0, 'tight-insert': 0,'target-fragment-size-bp':0,
                           'target-insert-size-kb': 0, 'target-template-size-bp': 0,
                           'degree-of-pooling': 0,'rna-protocol-option': 0,'rrna-depletion': 0,
                           'read-total': 0,'read-length-bp': 0,'plate-target-mass-lib-trial-ng': 0,
                           'tube-target-mass-lib-trial-ng': 0,'run-mode': 0,'library-creation-specs': 0,
                           'library-creation-queue': 0,'polyA-selection': 0,
                           'itag-primer-set': 0}

    #verify that all the expected attributes are returned in response of WS call
    print(" ")
    print("---- TEST 2: VERIFY THAT ALL EXPECTED ATTRIBUTES are RETRIEVED in response of WS call ----")

    for key, retrievedList in jsonstring.items():  #for each sow item template, check that  all attributes that are required (per contract) are in WS response
        if key == 'sow-item-templates':
            print("number of sow item templates = " + str(len(retrievedList)))
            print("")
            for sowItemTemplate in retrievedList:
                print("sow-number: " + str(sowItemTemplate.get('sow-number')))
                for attributeName in expectedAttributes:
                    if attributeName not in sowItemTemplate:
                        print("***Error. Not Found. Expecting attribute in sow Item Template = ", attributeName)

    print(" ")
    print("---- TEST 3: VERIFY THAT all the RETURNED ATTRIBUTES are the same as the expected attributes ----")
    # now go the other way and verify that all the returned  attributes are the same as the expected attributes (per contract)
    for key, retrievedList in jsonstring.items():
        if key == 'sow-item-templates':
            print("number of sow item templates = " + str(len(retrievedList)))
            print("")
            for sowItemTemplate in retrievedList:
                print("sow-number: " + str(sowItemTemplate.get('sow-number')))
                for attributeName in sowItemTemplate:
                    if attributeName not in expectedAttributes:
                        print("***Error, Unexpected attribute returned in sowItemTemplate = ", attributeName)

    # to do: confirm fields with values from database

    print(" ")
    #invalid sp-id , expect 404
    print("---- TEST 4: verify INVALID REQUEST - invalid SP ID,   EXPECT STATUS = 404 ----")
    spID = 0   # use invalid spid
    requestURL = 'http://claritydev1.jgi-psf.org/sequencing-projects/' + str(spID) + '/sow-item-templates'
    r = requests.get(requestURL)
    status = r.status_code
    if status == 404:
        print("Expected Returned status for invalid request (bad SP ID) = ", status)
    else:
        print("***Error, UnExpected Returned status for invalid request(bad SP ID) = ", status, ", Expected 404")

    print(" ")
    print("---- TEST 5: verify INVALID REQUEST - No SP ID,   EXPECT STATUS = 404 ----")
    # missing spid , expect 404
    requestURL = 'http://claritydev1.jgi-psf.org/sequencing-projects/sow-items-templates'
    r = requests.get(requestURL)
    status = r.status_code
    if status == 404:
        print("Expected Returned status for invalid request(missing SP ID) = ", status)

    else:
        print("***Error, UnExpected Returned status for invalid request(missing SP ID) = ", status, ", Expected 404")
        print("URL used: ", requestURL)

    print(" ")
    print("---- TEST 6: METHOD NOT ALLOWED - for POST, PUT,DELETE ,   EXPECT STATUS = 405 ----")
    # POST method , expect 405
    requestURL = 'http://claritydev1.jgi-psf.org/sequencing-projects/' + str(spID) + '/sow-item-templates'
    r = requests.post(requestURL)
    status = r.status_code
    if status == 405:
        print("Expected Returned status for invalid POST = ", status)

    else:
        print("***Error, UnExpected Returned status for invalid POST = ", status, ", Expected 405")
        print("URL used: ", requestURL)

    r = requests.put(requestURL)
    status = r.status_code
    if status == 405:
        print("Expected Returned status for invalid PUT = ", status)

    else:
        print("***Error, UnExpected Returned status for invalid PUT = ", status, ", Expected 405")
        print("URL used: ", requestURL)

        r = requests.post(requestURL)
        status = r.status_code

    if status == 405:
        print("Expected Returned status for invalid DELETE = ", status)

    else:
        print("***Error, UnExpected Returned status for invalid DELETE = ", status, ", Expected 405")
        print("URL used: ", requestURL)


## run the test
sowItemTemplatesWST()


#history -
# 6-30-17 this test was successful in finding 2 bugs with documentation
#      * created  PPS-3912 doc: retrieve sow-item templates for SP WS expected attribute not returned
#      * created PPS-3913 - doc: SowItemTemplates WS - UnExpected Returned status for invalid request(missing SP ID)